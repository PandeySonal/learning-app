package com.courseservice.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.courseservice.dto.CourseResponseDto;
import com.courseservice.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Integer> {

	List<Course> findByCourseNameContaining(String courseName);

}
