package com.courseservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "userservice")
public interface UserServiceClient {

	// Getting role by passing userId from USERSERVICE
	@RequestMapping(method = RequestMethod.GET, value = "/api/users/userId")
	public String getRoleByUserId(@RequestParam Integer userId);

}
