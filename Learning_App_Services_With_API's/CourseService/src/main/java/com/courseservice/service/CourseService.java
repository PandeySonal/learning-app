package com.courseservice.service;

import java.util.List;

import javax.validation.Valid;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.CourseResponseDto;

public interface CourseService {

	String createCourse(@Valid CourseRequestDto courseRequestDto, Integer userId);

	String updateCourse(@Valid CourseRequestDto courseRequestDto, Integer courseId, Integer userId);

	String deleteCourseByCourseId(Integer courseId, Integer userId);

	List<CourseResponseDto> getAllCourses();

	CourseResponseDto getCourseByCourseId(Integer courseId);

	List<CourseResponseDto> getCourseByCourseName(String courseName);

	

}
