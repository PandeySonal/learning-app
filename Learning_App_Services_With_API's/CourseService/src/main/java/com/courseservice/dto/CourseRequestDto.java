package com.courseservice.dto;

import com.courseservice.entity.CoursePrice;

public class CourseRequestDto {

	private String courseName;
	private String courseAuthor;
	private String courseCategory;
	private CoursePrice coursePrice;
	private Integer userId;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseAuthor() {
		return courseAuthor;
	}

	public void setCourseAuthor(String courseAuthor) {
		this.courseAuthor = courseAuthor;
	}

	public String getCourseCategory() {
		return courseCategory;
	}

	public void setCourseCategory(String courseCategory) {
		this.courseCategory = courseCategory;
	}

	public CoursePrice getCoursePrice() {
		return coursePrice;
	}

	public void setCoursePrice(CoursePrice coursePrice) {
		this.coursePrice = coursePrice;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
