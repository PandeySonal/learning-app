package com.courseservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.CourseResponseDto;
import com.courseservice.service.CourseService;

@RestController
public class CourseController {
	@Autowired
	CourseService courseService;

	// Api for creation of courses
	@PostMapping("/courses")
	public ResponseEntity<String> createCourse(@Valid @RequestBody CourseRequestDto courseRequestDto,
			@RequestParam Integer userId) {
		String response = courseService.createCourse(courseRequestDto, userId);
		return new ResponseEntity<String>(response, HttpStatus.CREATED);
	}

	// Api for updation of courses
	@PutMapping("/courses")
	public ResponseEntity<String> updateCourse(@Valid @RequestBody CourseRequestDto courseRequestDto,
			@RequestParam Integer courseId, @RequestParam Integer userId) {
		String response = courseService.updateCourse(courseRequestDto, courseId, userId);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

	// Api for deletion of courses
	@DeleteMapping("/courses")
	public ResponseEntity<String> deleteCourseByCourseId(@RequestParam Integer courseId, @RequestParam Integer userId) {
		String response = courseService.deleteCourseByCourseId(courseId, userId);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

	// Api for getting list of All courses
	@GetMapping("/courses")
	public ResponseEntity<List<CourseResponseDto>> getAllCourses() {
		return new ResponseEntity<List<CourseResponseDto>>(courseService.getAllCourses(), HttpStatus.OK);
	}

	// Api for getting course by passing courseId
	@GetMapping("/courses/courseId")
	public ResponseEntity<CourseResponseDto> getCourseByCourseId(@RequestParam Integer courseId) {
		return new ResponseEntity<CourseResponseDto>(courseService.getCourseByCourseId(courseId), HttpStatus.OK);
	}

	// Api for getting course by passing courseName
	@GetMapping("/courses/courseName")
	public ResponseEntity<List<CourseResponseDto>> getCourseByCourseName(@RequestParam String courseName) {
		return new ResponseEntity<List<CourseResponseDto>> (courseService.getCourseByCourseName(courseName), HttpStatus.OK);
	}

}
