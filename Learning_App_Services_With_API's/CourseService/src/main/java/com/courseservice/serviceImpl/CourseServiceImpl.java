package com.courseservice.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.CourseResponseDto;
import com.courseservice.entity.Course;
import com.courseservice.feign.UserServiceClient;
import com.courseservice.repo.CourseRepository;

import com.courseservice.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	CourseRepository courseRepository;
	@Autowired
	UserServiceClient userServiceClient;

	// Implementation code for creation of courses
	@Override
	public String createCourse(@Valid CourseRequestDto courseRequestDto, Integer userId) {
		if (userServiceClient.getRoleByUserId(userId).equalsIgnoreCase("TRAINER")) {
			Course course = new Course();
			BeanUtils.copyProperties(courseRequestDto, course);
			Course createdCourse = courseRepository.save(course);
			if (createdCourse != null)
				return " Course created successfully";
			return "Course creation was unsuccessfull";
		} else
			return "Create operation is not authorized for role=" + userServiceClient.getRoleByUserId(userId);
	}

	// Implementation code for updation of courses
	@Override
	public String updateCourse(@Valid CourseRequestDto courseRequestDto, Integer courseId, Integer userId) {
		if (userServiceClient.getRoleByUserId(userId).equalsIgnoreCase("TRAINER")) {
			Course course = courseRepository.findById(courseId).get();
			course.setUserId(course.getUserId());
			course.setCourseAuthor(courseRequestDto.getCourseAuthor());
			course.setCourseCategory(courseRequestDto.getCourseCategory());
			course.setCourseName(courseRequestDto.getCourseName());
			course.setCoursePrice(courseRequestDto.getCoursePrice());
			Course updatedCourse = courseRepository.save(course);
			if (updatedCourse != null)
				return " Course updated successfully";
			return "Course updation was unsuccessfull";
		} else
			return "Update operation is not authorized for role=" + userServiceClient.getRoleByUserId(userId);
	}

	// Implementation code for deletion of courses
	@Override
	public String deleteCourseByCourseId(Integer courseId, Integer userId) {
		if (userServiceClient.getRoleByUserId(userId).equalsIgnoreCase("TRAINER")) {
			Optional<Course> course = courseRepository.findById(courseId);
			if (course.isPresent()) {
				courseRepository.deleteById(courseId);
				return "Course deleted successfully";
			}
			return "Course deletion was unsuccessfull";
		} else
			return "Delete operation is not authorized for role=" + userServiceClient.getRoleByUserId(userId);

	}

	// Implementation code for getting all courses
	@Override
	public List<CourseResponseDto> getAllCourses() {
		List<CourseResponseDto> CourseResponseDtoList = new ArrayList<CourseResponseDto>();
		Iterator<Course> it = courseRepository.findAll().iterator();
		while (it.hasNext()) {
			CourseResponseDto courseResponseDto = new CourseResponseDto();
			BeanUtils.copyProperties(it.next(), courseResponseDto);
			CourseResponseDtoList.add(courseResponseDto);

		}
		return CourseResponseDtoList;
	}

	// Implementation code for getting Course by passing courseId
	@Override
	public CourseResponseDto getCourseByCourseId(Integer courseId) {
		Optional<Course> opCourse = courseRepository.findById(courseId);
		CourseResponseDto courseResponseDto = new CourseResponseDto();
		Course course = new Course();
		if (opCourse.isPresent()) {
			course = opCourse.get();
			BeanUtils.copyProperties(course, courseResponseDto);
			return courseResponseDto;
		}
		return null;

	}

	// Implementation code for getting Course by passing courseName
	@Override
	public List<CourseResponseDto> getCourseByCourseName(String courseName) {
		List<CourseResponseDto> CourseResponseDtoList = new ArrayList<CourseResponseDto>();
		Iterator<Course> it = courseRepository.findByCourseNameContaining(courseName).iterator();
		while (it.hasNext()) {
			CourseResponseDto courseResponseDto = new CourseResponseDto();
			BeanUtils.copyProperties(it.next(), courseResponseDto);
			CourseResponseDtoList.add(courseResponseDto);

		}
		return CourseResponseDtoList;
	}

}
