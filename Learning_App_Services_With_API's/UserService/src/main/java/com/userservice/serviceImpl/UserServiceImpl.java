package com.userservice.serviceImpl;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userservice.dto.LoginRequestDto;
import com.userservice.dto.UserRequestDto;
import com.userservice.entity.User;
import com.userservice.repo.UserRepository;
import com.userservice.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;

	// Implementation code for registering User
	@Override
	public String registerUser(UserRequestDto userRequestDto) {
		User user = new User();
		BeanUtils.copyProperties(userRequestDto, user);
		User savedUser = userRepository.save(user);
		if (savedUser != null)
			return "User data saved successfully";
		return "User data save was unsuccessfull";
	}

	// Implementation code for login User
	@Override
	public String loginUser(LoginRequestDto loginRequestDto) {

		Optional<User> opUser = userRepository.findByUserNameAndPassword(loginRequestDto.getUserName(),
				loginRequestDto.getPassword());
		if (opUser.isPresent()) {
			return "Hi " + loginRequestDto.getUserName() + "!! You're logged in succesfully ";
		} else {
			return "Unable to Login.Please enter valid credentials";
		}
	}

	// Implementation code for getting Role By passing UserId
	@Override
	public String getRoleByUserId(@Valid Integer userId) {
		Optional<User> opUser = userRepository.findById(userId);
		if (opUser.isPresent()) {
			User user = opUser.get();
			return user.getRole();
		}
		return "User not found";

	}

	// Implementation code for getting User By passing UserId
	@Override
	public User getUserByUserId(@Valid Integer userId) {
		User user = new User();
		Optional<User> opUser = userRepository.findById(userId);
		if (opUser.isPresent()) {
			 user = opUser.get();	
	       return user;
	}
	return null;}


}
