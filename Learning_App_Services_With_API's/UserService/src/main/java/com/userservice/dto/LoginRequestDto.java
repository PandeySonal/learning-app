package com.userservice.dto;

import javax.validation.constraints.NotNull;

public class LoginRequestDto {
	@NotNull(message= "userName should not be null")
	private String userName;
	@NotNull(message= "password should not be null")
	private String password;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}

