package com.userservice.dto;

import javax.validation.constraints.NotNull;

public class UserRequestDto {
	@NotNull(message= "fullName should not be null")
	private String fullName;
	@NotNull(message= "userName should not be null")
	private String userName;
	@NotNull(message= "password should not be null")
	private String password;
	private String role;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	

}
