package com.userservice.service;

import javax.validation.Valid;

import com.userservice.dto.LoginRequestDto;
import com.userservice.dto.UserRequestDto;
import com.userservice.entity.User;

public interface UserService {

	String registerUser(UserRequestDto userRequestDto);


	String loginUser(LoginRequestDto loginRequestDto);


	String getRoleByUserId(@Valid Integer userId);


	User getUserByUserId(@Valid Integer userId);

}