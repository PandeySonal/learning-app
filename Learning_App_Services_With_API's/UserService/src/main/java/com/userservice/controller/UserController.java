package com.userservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.userservice.dto.LoginRequestDto;
import com.userservice.dto.UserRequestDto;
import com.userservice.entity.User;
import com.userservice.service.UserService;

@RestController
@Validated
@RequestMapping("/api")
public class UserController {
	@Autowired
	UserService userService;

	// Api for registering User
	@PostMapping("/users")
	public ResponseEntity<String> registerUser(@Valid @RequestBody UserRequestDto userRequestDto) {
		String response = userService.registerUser(userRequestDto);
		return new ResponseEntity<String>(response, HttpStatus.CREATED);
	}

	// Api for login User
	@GetMapping("/users/login")
	public ResponseEntity<String> loginUser(@Valid @RequestBody LoginRequestDto loginRequestDto) {
		return new ResponseEntity<String>(userService.loginUser(loginRequestDto), HttpStatus.ACCEPTED);

	}

	// Api for getting role by passing userId
	@GetMapping("/users/userId")
	public ResponseEntity<String> getRoleByUserId(@Valid @RequestParam Integer userId) {
		return new ResponseEntity<String>(userService.getRoleByUserId(userId), HttpStatus.OK);

	}
	
	//Api for getting user by passing userId
	@GetMapping("/users/{userId}")
	public ResponseEntity<User> getUserByUserId(@Valid @PathVariable Integer userId) {
		return new ResponseEntity<User>(userService.getUserByUserId(userId), HttpStatus.OK);

	}
	
	
	
}
