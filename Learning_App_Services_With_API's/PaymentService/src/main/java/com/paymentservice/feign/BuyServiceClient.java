package com.paymentservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "buyservice")
public interface BuyServiceClient {

	@GetMapping("purchases/purchaseId")
	public String getStatusByPurchaseId(@RequestParam Integer purchaseId);

}
