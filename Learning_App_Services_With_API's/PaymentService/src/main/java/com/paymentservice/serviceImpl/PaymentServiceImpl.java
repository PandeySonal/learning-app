package com.paymentservice.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paymentservice.feign.BuyServiceClient;
import com.paymentservice.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	BuyServiceClient buyServiceClient;

	@Override
	public String doPayment(Integer purchaseId) {

		if (buyServiceClient.getStatusByPurchaseId(purchaseId).equalsIgnoreCase("PURCHASED")) {
			return "PAYMENT DONE SUCCESSFULLY";
		}
		return " PAYMENT NOT DONE FOR PURCHASEID " + purchaseId;

	}

}
