package com.paymentservice.service;

public interface PaymentService {

	String doPayment(Integer purchaseId);

}
