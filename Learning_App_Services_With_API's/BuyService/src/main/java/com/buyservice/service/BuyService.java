package com.buyservice.service;

import javax.validation.Valid;

import com.buyservice.dto.BuyRequestDto;

public interface BuyService {

	String buyCourse(@Valid Integer courseId, Integer userId);

	String getStatusByPurchaseId(@Valid Integer purchaseId);

}
