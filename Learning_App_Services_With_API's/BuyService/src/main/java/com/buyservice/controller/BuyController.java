package com.buyservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.buyservice.dto.BuyRequestDto;
import com.buyservice.service.BuyService;

@RestController
@Validated
public class BuyController {
	@Autowired
	BuyService buyService;

	// Api for buy Course by passing courseId And UserId
	@PostMapping("/purchases/{userId}")
	public ResponseEntity<String> buyCourse(@Valid @RequestParam Integer courseId, @PathVariable Integer userId) {
		String response = buyService.buyCourse(courseId, userId);
		return new ResponseEntity<String>(response, HttpStatus.CREATED);
	}

	//Api for getting status By purchaseId
	@GetMapping("purchases/purchaseId")
	public ResponseEntity<String> getStatusByPurchaseId(@Valid @RequestParam Integer purchaseId) {
		String response = buyService.getStatusByPurchaseId(purchaseId);
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
}
