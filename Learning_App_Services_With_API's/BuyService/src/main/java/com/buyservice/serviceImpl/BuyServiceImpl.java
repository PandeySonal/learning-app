package com.buyservice.serviceImpl;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.buyservice.dto.BuyRequestDto;
import com.buyservice.entity.Course;
import com.buyservice.entity.Purchase;
import com.buyservice.entity.User;
import com.buyservice.feign.CourseServiceClient;
import com.buyservice.feign.UserServiceClient;
import com.buyservice.repo.PurchaseRepository;
import com.buyservice.service.BuyService;

@Service
public class BuyServiceImpl implements BuyService {
	@Autowired
	CourseServiceClient courseServiceClient;
	@Autowired
	UserServiceClient userServiceClient;
	@Autowired
	PurchaseRepository purchaseRepository;

	// Implementation for buying course

	@Override
	public String buyCourse(@Valid Integer courseId, Integer userId) {
		User opUser = userServiceClient.getUserByUserId(userId);
		Course opCourse = courseServiceClient.getCourseByCourseId(courseId);
		if ((opUser == null) || (opCourse == null)) {
			return "Purchase Unsuccessfull";
		}
		Purchase purchase = new Purchase();
		purchase.setCourseId(courseId);
		purchase.setUserId(userId);
		purchase.setDateTime(LocalDateTime.now());
		purchase.setStatus("PURCHASED");
		purchaseRepository.save(purchase);
		return opCourse.getCourseName() + " Course purchased Successfully by  " + opUser.getFullName();
	}

	@Override
	public String getStatusByPurchaseId(@Valid Integer purchaseId) {
		Optional<Purchase> opPurchase = purchaseRepository.findById(purchaseId);
		if (opPurchase.isPresent()) {
			Purchase purchase = opPurchase.get();
			return purchase.getStatus();
		}
		return "purchase not done with given purchaseId" ;
	}

}
