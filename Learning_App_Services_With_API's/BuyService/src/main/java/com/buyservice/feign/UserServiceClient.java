package com.buyservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.buyservice.entity.User;

@FeignClient(name = "userservice")
public interface UserServiceClient {

	// Getting User by passing userId from USERSERVICE
	
	@GetMapping("/api/users/{userId}")
	public User getUserByUserId(@PathVariable Integer userId);

}
