package com.buyservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.buyservice.entity.Course;

@FeignClient(name = "courseservice")
public interface CourseServiceClient {

	// Getting course by passing courseId from COURSESERVICE

	@GetMapping("/courses/courseId")
	public Course getCourseByCourseId(@RequestParam Integer courseId);

}
