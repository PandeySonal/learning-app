package com.paymentservice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import com.paymentservice.dto.PaymentResponseDto;
import com.paymentservice.service.PaymentService;

@ExtendWith(MockitoExtension.class)
public class PaymentControllerTest {

	@Mock
	PaymentService paymentService;
	@InjectMocks
	PaymentController paymentController;
	PaymentResponseDto response;

	@BeforeEach
	void setUp() {
		response = new PaymentResponseDto();
		response.setMessage("PAYMENT DONE SUCCESSFULLY");

	}

	@Test
	@DisplayName("Get Purchase Satus: Positive Scenario")
	void testGetPurchaseStatusByPurchaseId()  {
		// given
		when(paymentService.doPayment(7)).thenReturn(response);
		// when
		ResponseEntity<PaymentResponseDto> responseDto = paymentController.doPayment(7);
		// then
		assertEquals(response.getMessage(), responseDto.getBody().getMessage());
		assertNotNull(responseDto);

	}

}
