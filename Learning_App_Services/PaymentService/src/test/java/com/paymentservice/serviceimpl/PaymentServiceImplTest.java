package com.paymentservice.serviceimpl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.paymentservice.dto.PaymentResponseDto;
import com.paymentservice.entity.Purchase;
import com.paymentservice.exception.PurchaseNotFoundException;
import com.paymentservice.repo.PaymentRepository;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceImplTest {

	@Mock
	PaymentRepository paymentRepository;
	@InjectMocks
	PaymentServiceImpl paymentServiceImpl;
	private static final String PURCHASE = "PAYMENT DONE SUCCESSFULLY";
	private static final String NOTFOUND = "Payment Not Found For PurchaseId";
	Purchase purchase;
	Optional<Purchase> opPurchase;
	private Integer purchaseId = 7;

	@BeforeEach
	void setUp() {
		purchase = new Purchase();
		purchase.setPurchaseId(7);
		purchase.setUserId(2);
		purchase.setCourseId(1);
		purchase.setDateTime(null);
		purchase.setStatus("PURCHASED");
		opPurchase = Optional.of(purchase);
	}

	@Test
	@DisplayName("Do Payment: Positive Scenario")
	void afterChekingStatusShouldDoPayment() {
		// given
		when(paymentRepository.findById(7)).thenReturn(opPurchase);
		// when
		PaymentResponseDto responseDto = paymentServiceImpl.doPayment(7);
		// then
		assertEquals(PURCHASE, responseDto.getMessage());
		assertNotNull(responseDto);

	}

	@Test
	@DisplayName("Do Payment: Negative Scenario")
	void afterChekingStatusShouldThrowPurchaseNotFoundException() throws PurchaseNotFoundException {
		// given
		when(paymentRepository.findById(purchaseId)).thenReturn(Optional.empty());
		// when
		Exception exception = assertThrows(PurchaseNotFoundException.class, () -> {
			paymentServiceImpl.doPayment(purchaseId);
		});

		// then
		String expectedMessage = NOTFOUND;
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

}
