package com.paymentservice.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.paymentservice.dto.PaymentResponseDto;
import com.paymentservice.service.PaymentService;
import com.paymentservice.serviceimpl.PaymentServiceImpl;

@RestController
@RequestMapping("/payment")
public class PaymentController {

	public static final Logger logger = Logger.getLogger(PaymentController.class);
	@Autowired
	PaymentService paymentService;

	// Getting payment status by passing purchaseId
	@HystrixCommand(fallbackMethod = "checkStatus")
	@GetMapping("/payments")
	public ResponseEntity<PaymentResponseDto> doPayment(@RequestParam Integer purchaseId) {
		logger.info(PaymentServiceImpl.STATUS);
		// return new
		// ResponseEntity<PaymentResponseDto>(paymentService.doPayment(purchaseId),HttpStatus.OK);
		throw new RuntimeException("Not Available");
	}

	public ResponseEntity<PaymentResponseDto> checkStatus(@RequestParam Integer purchaseId) {
		return new ResponseEntity<PaymentResponseDto>(paymentService.doPayment(purchaseId), HttpStatus.OK);
	}
}