package com.paymentservice.serviceimpl;

import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.paymentservice.dto.PaymentResponseDto;
import com.paymentservice.entity.Purchase;
import com.paymentservice.exception.PurchaseNotFoundException;
import com.paymentservice.repo.PaymentRepository;
import com.paymentservice.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {
	public static final Logger logger = Logger.getLogger(PaymentServiceImpl.class);
	public static final String STATUS = "PAYMENT DONE SUCCESSFULLY";
	private static final String NOTFOUND = "Payment Not Found For PurchaseId";
	private static final String PLACE = "PURCHASED";
	@Autowired
	PaymentRepository paymentRepository;
	PaymentResponseDto paymentResponseDto = new PaymentResponseDto();

	// Do payment for selected course by passing purchaseId

	@Override
	public PaymentResponseDto doPayment(Integer purchaseId) throws PurchaseNotFoundException {
		Optional<Purchase> opPurchase = paymentRepository.findById(purchaseId);
		if (opPurchase.isEmpty()) {
			logger.error(NOTFOUND);
			throw new PurchaseNotFoundException(NOTFOUND);
		} else if (opPurchase.get().getStatus().equalsIgnoreCase(PLACE)) {
			paymentResponseDto.setMessage(STATUS);
			logger.info(STATUS);
		}
		return paymentResponseDto;
	}

}
