package com.paymentservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paymentservice.entity.Purchase;

@Repository
public interface PaymentRepository extends JpaRepository<Purchase, Integer> {

}
