package com.paymentservice.service;

import com.paymentservice.dto.PaymentResponseDto;

public interface PaymentService {

	PaymentResponseDto doPayment(Integer purchaseId);

}
