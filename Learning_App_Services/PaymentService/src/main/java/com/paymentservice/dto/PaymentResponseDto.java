package com.paymentservice.dto;

import java.io.Serializable;

public class PaymentResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PaymentResponseDto(String message) {
		super();
		this.message = message;
	}

	public PaymentResponseDto() {
		super();

	}

}
