package com.paymentservice.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class PaymentRequestDto implements Serializable{

	private static final long serialVersionUID = 1L;
	@NotNull(message="purchaseId cannot be null")
	private Integer purchaserId;

	public Integer getPurchaserId() {
		return purchaserId;
	}

	public void setPurchaserId(Integer purchaserId) {
		this.purchaserId = purchaserId;
	}
	
}
