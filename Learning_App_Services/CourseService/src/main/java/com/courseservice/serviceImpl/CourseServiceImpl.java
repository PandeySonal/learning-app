package com.courseservice.serviceImpl;

import java.util.Optional;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.ResponseDto;
import com.courseservice.entity.Course;
import com.courseservice.exception.CourseNotFoundException;
import com.courseservice.repo.CourseRepository;
import com.courseservice.service.CourseService;

//Class implementing the methods of CourseService interface.

@Service
public class CourseServiceImpl implements CourseService {
	public static final Logger log = Logger.getLogger(CourseServiceImpl.class);
	@Autowired
	CourseRepository courseRepository;
	ResponseDto responseDto = new ResponseDto();
	private static final String SUCCESS = "Operation Successfull";
	private static final String NOTFOUND = "Course not found";
	private static final String CREATE = "COURSE CREATED";
	private static final String UPDATE = "COURSE UPDATED";
	private static final String DELETE = "COURSE DELETED";

	// Method for creation of Course.
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public ResponseDto createCourse(@Valid CourseRequestDto courseRequestDto) {
		Course course = new Course();
		mapCourseDto(courseRequestDto, course);
		courseRepository.save(course);
		log.info(CREATE);
		responseDto.setStatus(SUCCESS);
		return responseDto;
	}

	// Method for Updation of Course.
	@ResponseStatus(HttpStatus.OK)
	@Override
	public ResponseDto updateCourse(@Valid CourseRequestDto courseRequestDto, Integer courseId) {
		Optional<Course> opCourse = courseRepository.findById(courseId);
		if (opCourse.isEmpty()) {
			log.error(NOTFOUND);
			throw new CourseNotFoundException(NOTFOUND);
		} else {
			Course course = opCourse.get();
			course.setUserId(courseRequestDto.getUserId());
			course.setCourseAuthor(courseRequestDto.getCourseAuthor());
			course.setCourseCategory(courseRequestDto.getCourseCategory());
			course.setCourseName(courseRequestDto.getCourseName());
			course.setCoursePrice(courseRequestDto.getCoursePrice());
			courseRepository.save(course);
			log.info(String.format(" %s WITH ID %d ", UPDATE, courseId));
			responseDto.setStatus(SUCCESS);
		}
		return responseDto;

	}

	// Method for Deletion of Course.
	@ResponseStatus(HttpStatus.OK)
	@Override
	public ResponseDto deleteCourseByCourseId(Integer courseId) {
		Optional<Course> opCourse = courseRepository.findById(courseId);
		if (opCourse.isEmpty()) {
			log.error(NOTFOUND);
			throw new CourseNotFoundException(NOTFOUND);

		} else {
			courseRepository.deleteById(courseId);
			log.info(String.format("%s WITH ID %d ", DELETE, courseId));
			responseDto.setStatus(SUCCESS);
		}
		return responseDto;
	}

	// Mapping from CourseRequestDto to course
	private void mapCourseDto(CourseRequestDto courseRequestDto, Course course) {
		course.setCourseName(courseRequestDto.getCourseName());
		course.setCourseAuthor(courseRequestDto.getCourseAuthor());
		course.setCourseCategory(courseRequestDto.getCourseCategory());
		course.setUserId(courseRequestDto.getUserId());
		course.setCoursePrice(courseRequestDto.getCoursePrice());
	}

}
