package com.courseservice.service;

import javax.validation.Valid;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.ResponseDto;

//Service interface for CourseService having methods for create,update & delete.

public interface CourseService {

	ResponseDto createCourse(@Valid CourseRequestDto courseRequestDto);

	ResponseDto updateCourse(@Valid CourseRequestDto courseRequestDto, Integer courseId);

	ResponseDto deleteCourseByCourseId(@Valid Integer courseId);

}
