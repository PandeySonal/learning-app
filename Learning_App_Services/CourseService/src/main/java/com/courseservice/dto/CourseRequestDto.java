package com.courseservice.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.courseservice.entity.CoursePrice;

//CourseRequestDto consists of parameters (with getters & setters) to be passed while sending Http requests.

public class CourseRequestDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull(message = "CourseName cannot be null")
	private String courseName;
	@NotNull(message = "CourseAuthor cannot be null")
	private String courseAuthor;
	@NotNull(message = "CourseCategory cannot be null")
	private String courseCategory;
	@NotNull(message = "CoursePrice cannot be null")
	private CoursePrice coursePrice;
	@NotNull(message = "UserId cannot be null")
	private Integer userId;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseAuthor() {
		return courseAuthor;
	}

	public void setCourseAuthor(String courseAuthor) {
		this.courseAuthor = courseAuthor;
	}

	public String getCourseCategory() {
		return courseCategory;
	}

	public void setCourseCategory(String courseCategory) {
		this.courseCategory = courseCategory;
	}

	public CoursePrice getCoursePrice() {
		return coursePrice;
	}

	public void setCoursePrice(CoursePrice coursePrice) {
		this.coursePrice = coursePrice;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
