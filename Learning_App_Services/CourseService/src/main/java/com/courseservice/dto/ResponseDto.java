package com.courseservice.dto;

import java.io.Serializable;

//ResponseDto returns Status as response after Http requests are fulfilled.

public class ResponseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ResponseDto() {
		super();

	}

}
