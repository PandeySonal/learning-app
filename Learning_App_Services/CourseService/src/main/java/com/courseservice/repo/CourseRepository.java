package com.courseservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.courseservice.entity.Course;

// Repository interface for CourseService extending JPA Repository.

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

}
