package com.courseservice.controller;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.ResponseDto;
import com.courseservice.service.CourseService;

// CourseController class consists of three API's for create,update & delete courses.

@RequestMapping("/course")
@RestController
public class CourseController {
	public static final Logger log = Logger.getLogger(CourseController.class);
	private static final String CREATE = "Getting Request for COURSE CREATION";
	private static final String UPDATE = "Getting Request for COURSE UPDATION";
	private static final String DELETE = "Getting Request for COURSE DELETION";
	@Autowired
	CourseService courseService;

	// Api for creation of courses.
	@PostMapping
	public ResponseEntity<ResponseDto> createCourse(@Valid @RequestBody CourseRequestDto courseRequestDto) {
		log.info(CREATE);
		return new ResponseEntity<>(courseService.createCourse(courseRequestDto), HttpStatus.CREATED);

	}

	// Api for updation of courses.
	@PutMapping
	public ResponseEntity<ResponseDto> updateCourse(@Valid @RequestBody CourseRequestDto courseRequestDto,
			@RequestParam Integer courseId) {
		log.info(UPDATE);
		return new ResponseEntity<>(courseService.updateCourse(courseRequestDto, courseId), HttpStatus.OK);
	}

	// Api for deletion of courses.
	@DeleteMapping
	public ResponseEntity<ResponseDto> deleteCourseByCourseId(@Valid @RequestParam Integer courseId) {
		log.info(DELETE);
		return new ResponseEntity<>(courseService.deleteCourseByCourseId(courseId), HttpStatus.OK);
	}

}
