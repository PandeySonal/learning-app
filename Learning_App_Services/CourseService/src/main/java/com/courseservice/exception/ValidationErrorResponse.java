package com.courseservice.exception;

import java.util.HashMap;
import java.util.Map;

//ValidationErrorResponse class is used for returning errors in the form of key-value and is used by MethodArguementNotValidException class.

public class ValidationErrorResponse extends ErrorResponse {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, String> errors = new HashMap<>();

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

}
