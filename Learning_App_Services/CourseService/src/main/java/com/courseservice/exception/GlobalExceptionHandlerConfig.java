package com.courseservice.exception;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

// Global Exception Handling class where all type of exceptions are configured.

@RestControllerAdvice
public class GlobalExceptionHandlerConfig {

	private static final String INVALID = "INVALID VALUES ENTERED AS INPUT";
	private static final String INVALIDJSON = "INVALID JSON VALUES ENTERED AS INPUT";

	// Configuration of CourseNotFoundException.
	@ExceptionHandler(CourseNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleException(CourseNotFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setTime(LocalDateTime.now());
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setResponseCode(HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	// Configuration of MethodArguementNotValidException.
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationErrorResponse> handleException(MethodArgumentNotValidException ex) {
		List<FieldError> errors = ex.getFieldErrors();
		ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
		for (FieldError err : errors) {
			validationErrorResponse.getErrors().put(err.getField(), err.getDefaultMessage());
		}
		validationErrorResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
		validationErrorResponse.setMessage(INVALID);
		validationErrorResponse.setTime(LocalDateTime.now());
		return new ResponseEntity<>(validationErrorResponse, HttpStatus.BAD_REQUEST);
	}

	// Configuration of HttpMessageNotReadableException.

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<JsonErrorResponse> handleException(HttpMessageNotReadableException ex) {
		JsonErrorResponse errorResponse = new JsonErrorResponse();
		errorResponse.setTime(LocalDateTime.now());
		errorResponse.setMessage(INVALIDJSON);
		errorResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
		errorResponse.setCause(ex.getCause().toString());
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

}
