package com.courseservice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.ResponseDto;
import com.courseservice.entity.CoursePrice;
import com.courseservice.service.CourseService;

//Test Class for Controller

@ExtendWith(MockitoExtension.class)
class CourseControllerTest {
	@Mock
	CourseService courseService;
	@InjectMocks
	CourseController courseController;
	CourseRequestDto courseRequestDto;
	CourseRequestDto courseRequestDto1;
	ResponseDto responseDto;
	ResponseDto responseDto1;
	private static final String SUCCESS = "Operation Successfull";
	private Integer courseId;

	@BeforeEach
	void setUp() {
		courseRequestDto = new CourseRequestDto();
		CoursePrice coursePrice = new CoursePrice();
		coursePrice.setCurrency("INR");
		coursePrice.setPrice(12000.00);
		courseRequestDto.setUserId(10);
		courseRequestDto.setCourseName("Java Full Stack");
		courseRequestDto.setCourseCategory("Programming");
		courseRequestDto.setCoursePrice(coursePrice);
		courseRequestDto.setCourseAuthor("Arya Singh");
		courseId = 20;
		courseRequestDto1 = new CourseRequestDto();
		courseRequestDto1 = null;
		responseDto = new ResponseDto();
		responseDto.setStatus(SUCCESS);

	}

	// Positive test case for CourseCreation API in controller
	@Test
	@DisplayName("CreateCourse")
	void testCreateCourseShouldReturnResponseDtoAsSuccess() {
		// given
		when(courseService.createCourse(courseRequestDto)).thenReturn(responseDto);
		// when
		ResponseEntity<ResponseDto> responseDto2 = courseController.createCourse(courseRequestDto);
		// then
		assertNotNull(responseDto2);
		assertEquals(responseDto, responseDto2.getBody());
		assertEquals(HttpStatus.CREATED, responseDto2.getStatusCode());
	}

	// Positive test case for CourseUpdation API in controller
	@Test
	@DisplayName("UpdateCourse")
	void testUpdateCourseShouldReturnResponseDtoAsSuccess() {
		// given
		when(courseService.updateCourse(courseRequestDto, courseId)).thenReturn(responseDto);
		// when
		ResponseEntity<ResponseDto> responseDto2 = courseController.updateCourse(courseRequestDto, courseId);
		// then
		assertNotNull(responseDto2);
		assertEquals(responseDto, responseDto2.getBody());
		assertEquals(HttpStatus.OK, responseDto2.getStatusCode());
	}

	// Positive test case for CourseDeletion API in controller
	@Test
	@DisplayName("DeleteCourse")
	void testDeleteCourseByCourseIdShouldReturnResponseDtoAsSuccess() {
		// given
		when(courseService.deleteCourseByCourseId(courseId)).thenReturn(responseDto);
		// when
		ResponseEntity<ResponseDto> responseDto2 = courseController.deleteCourseByCourseId(courseId);
		// then
		assertNotNull(responseDto2);
		assertEquals(responseDto, responseDto2.getBody());
		assertEquals(HttpStatus.OK, responseDto2.getStatusCode());
	}

}
