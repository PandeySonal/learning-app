package com.courseservice.serviceImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;

import com.courseservice.dto.CourseRequestDto;
import com.courseservice.dto.ResponseDto;
import com.courseservice.entity.Course;
import com.courseservice.entity.CoursePrice;
import com.courseservice.exception.CourseNotFoundException;
import com.courseservice.repo.CourseRepository;

//Test Class for ServiceImpl

@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

	@Mock
	CourseRepository courseRepository;

	@InjectMocks
	CourseServiceImpl courseServiceImpl;

	CourseRequestDto courseRequestDto;
	private Integer courseId;
	Course course;
	Course savedCourse;
	Course updatedCourse;
	Course emptyCourse;
	Optional<Course> opCourse;
	Optional<Course> opCourse1;
	private static final String SUCCESS = "Operation Successfull";
	private static final String NOTFOUND = "Course not found";

	@BeforeEach
	void setUp() {
		courseRequestDto = new CourseRequestDto();
		CoursePrice coursePrice = new CoursePrice();
		coursePrice.setCurrency("INR");
		coursePrice.setPrice(12000.00);
		courseRequestDto.setUserId(10);
		courseRequestDto.setCourseName("Java Full Stack");
		courseRequestDto.setCourseCategory("Programming");
		courseRequestDto.setCoursePrice(coursePrice);
		courseRequestDto.setCourseAuthor("Arya Singh");
		courseId = 20;
		course = new Course();
		CoursePrice coursePrice1 = new CoursePrice();
		coursePrice1.setCurrency("INR");
		coursePrice1.setPrice(12000.00);
		course.setCourseAuthor("Arya Singh");
		course.setCourseCategory("Programming");
		course.setCourseName("Java Full Stack");
		course.setCoursePrice(coursePrice1);
		course.setUserId(10);
		course.setCourseId(courseId);
		savedCourse = new Course();
		CoursePrice coursePrice2 = new CoursePrice();
		coursePrice2.setCurrency("INR");
		coursePrice2.setPrice(12000.00);
		savedCourse.setCourseAuthor("Arya Singh");
		savedCourse.setCourseCategory("Programming");
		savedCourse.setCourseName("Java Full Stack");
		savedCourse.setCoursePrice(coursePrice2);
		savedCourse.setUserId(10);
		savedCourse.setCourseId(courseId);
		opCourse = Optional.of(course);
		updatedCourse = new Course();
		updatedCourse.setCourseAuthor("Arya");
		updatedCourse.setCourseCategory("Programming");
		updatedCourse.setCourseId(21);
		updatedCourse.setCourseName("Java");
		updatedCourse.setCoursePrice(coursePrice2);

	}

	// Positive test case for CourseCreation method in serviceImpl
	@Test
	@DisplayName("CreateCourse:Positive Scenario")
	void testCreateCourseShouldReturnResponseDtoAsSuccess() {
		// given
		when(courseRepository.save(any(Course.class))).thenReturn(savedCourse);
		// when
		ResponseDto response1 = courseServiceImpl.createCourse(courseRequestDto);
		// then
		assertNotNull(response1);
		assertEquals(SUCCESS, response1.getStatus());

	}

	// Positive test case for CourseUpdation method in serviceImpl
	@Test
	@DisplayName("UpdateCourse:Positive Scenario")
	void testUpdateCourseShouldReturnResponseDtoAsSuccess() {
		// given
		when(courseRepository.findById(courseId)).thenReturn(opCourse);
		when(courseRepository.save(any(Course.class))).thenReturn(updatedCourse);
		// when
		ResponseDto response1 = courseServiceImpl.updateCourse(courseRequestDto, courseId);
		// then
		assertNotNull(response1);
		assertEquals(SUCCESS, response1.getStatus());

	}

	// Positive test case for CourseDeletion method in serviceImpl
	@Test
	@DisplayName("DeleteCourse:Positive Scenario")
	void testDeleteCourseShouldReturnResponseDtoAsSuccess() {
		// given
		when(courseRepository.findById(courseId)).thenReturn(opCourse);
		doNothing().when(courseRepository).deleteById(courseId);
		// when
		ResponseDto response1 = courseServiceImpl.deleteCourseByCourseId(courseId);
		// then
		assertNotNull(response1);
		assertEquals(SUCCESS, response1.getStatus());

	}

	// Negative test case for CourseUpdation method in serviceImpl
	@Test
	@DisplayName("UpdateCourse:Negative Scenario")
	void testUpdateCourseShouldThrowCourseNotFoundExceptionWhenCourseNotFound() {
		// given
		when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

		// when
		Exception exception = assertThrows(CourseNotFoundException.class, () -> {
			courseServiceImpl.updateCourse(courseRequestDto, courseId);
		});
		// then
		String expectedMessage = NOTFOUND;
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));

	}

	// Negative test case for CourseDeletion method in serviceImpl
	@Test
	@DisplayName("DeleteCourse:Negative Scenario")
	void testDeleteCourseShouldThrowCourseNotFoundExceptionWhenCourseNotFound() throws CourseNotFoundException {
		// given
		when(courseRepository.findById(courseId)).thenReturn(Optional.empty());
		// when
		Exception exception = assertThrows(CourseNotFoundException.class, () -> {
			courseServiceImpl.deleteCourseByCourseId(courseId);
		});
		// then
		String expectedMessage = NOTFOUND;
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));

	}

}
