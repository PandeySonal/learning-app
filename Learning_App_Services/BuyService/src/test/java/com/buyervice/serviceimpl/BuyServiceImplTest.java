package com.buyervice.serviceimpl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.buyservice.dto.BuyRequestDto;
import com.buyservice.dto.BuyResponseDto;
import com.buyservice.entity.Purchase;
import com.buyservice.exception.CourseNotPurchasedException;
import com.buyservice.repo.PurchaseRepository;
import com.buyservice.serviceimpl.BuyServiceImpl;

@ExtendWith(MockitoExtension.class)
public class BuyServiceImplTest {

	@Mock
	PurchaseRepository purchaseRepository;
	@InjectMocks
	BuyServiceImpl buyServiceImpl;
	BuyRequestDto buyRequestDto;
	private static final String STATUS = "COURSE PURCHASED SUCCESFULLY";
	Purchase purchase;
	Optional<Purchase> opPurchase;
	Optional<Purchase> coursePurchase;

	@BeforeEach
	void setUp() throws Exception {
		purchase = new Purchase();
		purchase.setCourseId(1);
		purchase.setUserId(2);
		purchase.setPurchaseId(7);
		purchase.setStatus("PURCHASED");
		buyRequestDto = new BuyRequestDto();
		buyRequestDto.setCourseId(1);
		buyRequestDto.setUserId(2);
		opPurchase = Optional.of(purchase);
		
	}

	@Test
	@DisplayName("Buy course : Positive Scenario")
	void testBuyCourseByPassingCourseIdAndUserId() {
		// given
		when(purchaseRepository.save(any(Purchase.class))).thenReturn(purchase);
		// when
		BuyResponseDto buyResponseDto = buyServiceImpl.buyCourse(buyRequestDto);
		// then
		assertNotNull(buyResponseDto);
		assertEquals(STATUS, buyResponseDto.getStatus());
	}

	@Test
	@DisplayName("Get purchase status: Positive Scenario")
	void testGetPurchaseStatusByPurchaseId() {
		// given
		when(purchaseRepository.findById(7)).thenReturn(opPurchase);
		// when
		BuyResponseDto buyResponseDto = buyServiceImpl.getStatusByPurchaseId(7);
		// then
		assertEquals(STATUS, buyResponseDto.getStatus());
		assertNotNull(buyResponseDto);
	}

	@Test
	@DisplayName("Get purchase status: Negative scenario")
	void testGetPurchaseStatusByPurchaseIdShouldReturnCourseNotPurchasedException() throws CourseNotPurchasedException {
		// given
		when(purchaseRepository.findById(7)).thenReturn(Optional.empty());
		// when
		Exception exception = assertThrows(CourseNotPurchasedException.class, () -> {
			buyServiceImpl.getStatusByPurchaseId(7);
		});
		// then
		String expectedMessage = "COURSE NOT PURCHASED FOR GIVEN PURCHASEID";
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
}
