package com.buyervice.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import com.buyservice.controller.BuyController;
import com.buyservice.dto.BuyRequestDto;
import com.buyservice.dto.BuyResponseDto;
import com.buyservice.service.BuyService;

@ExtendWith(MockitoExtension.class)
public class BuyControllerTest {

	@Mock
	BuyService buyService;
	@InjectMocks
	BuyController buyController;
	BuyResponseDto response;
	BuyRequestDto request;
	private Integer purchaseId;
	
	@BeforeEach
	void setUp() throws Exception {
		
		request=new BuyRequestDto();
		request.setCourseId(1);
		request.setUserId(2);
		response=new BuyResponseDto();
		response.setStatus("COURSE PURCHASED SUCCESFULLY");	
		purchaseId=7;
	}
	@Test
	@DisplayName("Buy Course: Positive Scenario")
	void testBuyCourseByPassingCourseIdAndUserId() {
		//given
		when(buyService.buyCourse(request)).thenReturn(response);
		//when
		ResponseEntity<BuyResponseDto> responseDto=buyController.buyCourse(request);
		//then
		assertEquals(response, responseDto.getBody());
		assertNotNull(responseDto);
		}
	
	@Test
	@DisplayName("Getting purchase status by purchaseId :Positive Scenario")
	void testGetPurchaseStatusByPurchaseId() {
		//given
		when(buyService.getStatusByPurchaseId(purchaseId)).thenReturn(response);
		//when
		ResponseEntity<BuyResponseDto> responseDto1=buyController.getStatusByPurchaseId(purchaseId);
		//then
		assertEquals(response, responseDto1.getBody());
		assertNotNull(responseDto1);
	}
	
}
