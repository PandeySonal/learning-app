package com.buyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;


@SpringBootApplication
@EnableEncryptableProperties
public class BuyServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuyServiceApplication.class, args);
	}

}
