package com.buyservice.serviceimpl;

import java.time.LocalDateTime;
import java.util.Optional;
import javax.validation.Valid;
import org.jboss.logging.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.buyservice.dto.BuyRequestDto;
import com.buyservice.dto.BuyResponseDto;
import com.buyservice.entity.Purchase;
import com.buyservice.exception.CourseNotPurchasedException;
import com.buyservice.repo.PurchaseRepository;
import com.buyservice.service.BuyService;

@Service
public class BuyServiceImpl implements BuyService {
	@Autowired
	PurchaseRepository purchaseRepository;
	public static final Logger logger=Logger.getLogger(BuyServiceImpl.class);

	public static final String STATUS="COURSE PURCHASED SUCCESFULLY";
	private static final String  NOTFOUND="COURSE NOT PURCHASED FOR GIVEN PURCHASEID";
	private static final String  PLACE="PURCHASED";
	BuyResponseDto buyResponseDto = new BuyResponseDto();
	
	// This method used for user can buy the course
	
	@Override
	public BuyResponseDto buyCourse(@Valid BuyRequestDto buyRequestDto) {
		Purchase purchase= new Purchase();
		purchase.setCourseId(buyRequestDto.getCourseId());
		purchase.setUserId(buyRequestDto.getUserId());
		purchase.setDateTime(LocalDateTime.now());
		purchase.setStatus(PLACE);
		purchaseRepository.save(purchase);
		buyResponseDto.setStatus(STATUS);
		logger.info(STATUS);
		return buyResponseDto;
	}
	
    // Getting the purchase status by passing purchaseId
	
	@Override
	public BuyResponseDto getStatusByPurchaseId(@Valid Integer purchaseId) {
		Optional<Purchase> opPurchase = purchaseRepository.findById(purchaseId);
		Purchase purchase;
		if(opPurchase.isEmpty()) {
			logger.error(NOTFOUND);
			throw new CourseNotPurchasedException(NOTFOUND);
		}
		purchase=opPurchase.get();
		BeanUtils.copyProperties(opPurchase, purchase);
		buyResponseDto.setStatus(STATUS);
		logger.info(STATUS);
		return buyResponseDto;	
	}
	
}

	

