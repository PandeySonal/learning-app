package com.buyservice.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.buyservice.dto.BuyRequestDto;
import com.buyservice.dto.BuyResponseDto;
import com.buyservice.service.BuyService;
import com.buyservice.serviceimpl.BuyServiceImpl;
@RestController
@Validated
public class BuyController {
    @Autowired
	BuyService buyService ;
    public static final org.jboss.logging.Logger logger=org.jboss.logging.Logger.getLogger(BuyController.class);
    public static final String MESSAGE="Buy Course";
 
    // API for buy Course by passing courseId And UserId
    
	@PostMapping("/purchases")
	public ResponseEntity<BuyResponseDto> buyCourse(@Valid @RequestBody BuyRequestDto buyRequestDto ) {
	logger.info(MESSAGE);
	return new ResponseEntity<BuyResponseDto>(buyService.buyCourse(buyRequestDto), HttpStatus.CREATED);
	}
	
	//API for getting status By purchaseId
	
	@GetMapping("/purchases/purchaseId")
	public ResponseEntity<BuyResponseDto> getStatusByPurchaseId(@Valid @RequestParam Integer purchaseId){
	logger.info(BuyServiceImpl.STATUS);
	return new ResponseEntity<BuyResponseDto>(buyService.getStatusByPurchaseId(purchaseId), HttpStatus.OK);
		

	}
	
}
