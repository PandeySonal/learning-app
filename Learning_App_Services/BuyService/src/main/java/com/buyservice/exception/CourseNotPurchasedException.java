package com.buyservice.exception;


public class CourseNotPurchasedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CourseNotPurchasedException(String message) {
		super(message);
	}

}
