package com.buyservice.service;

import javax.validation.Valid;
import com.buyservice.dto.BuyRequestDto;
import com.buyservice.dto.BuyResponseDto;

public interface BuyService {

	BuyResponseDto buyCourse(@Valid BuyRequestDto buyRequestDto);

	BuyResponseDto getStatusByPurchaseId(@Valid Integer purchaseId);

}
