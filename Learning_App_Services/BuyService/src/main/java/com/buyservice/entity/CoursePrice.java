package com.buyservice.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CoursePrice implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Double price;
	private String currency;
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	

}
