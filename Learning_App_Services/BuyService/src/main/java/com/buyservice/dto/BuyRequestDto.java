package com.buyservice.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class BuyRequestDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    @NotNull(message="CourseId cannot be null")
	private Integer courseId;
    @NotNull(message="UserId cannot be null")
	private Integer userId;
	public Integer getCourseId() {
		return courseId;
	}
	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
}
